# zfw-ui

### 在线演示

暂无 :grin: 

### 项目介绍

- zfw-ui是基于Vue、Element实现的Java快速开发平台的前端。

- 目标是搭建出一套简洁易用的快速解决方案，可以帮助用户有效降低项目开发难度和成本。

### 功能列表

- ✔ 系统登录：系统用户登录，系统登录认证（token方式）
- ✔ 用户管理：新建用户，修改用户，删除用户，查询用户
- ✔ 机构管理：新建机构，修改机构，删除机构，查询机构
- ✔ 角色管理：新建角色，修改角色，删除角色，查询角色
- ✔ 菜单管理：新建菜单，修改菜单，删除菜单，查询菜单
- ✔ 字典管理：新建字典，修改字典，删除字典，查询字典
- ✔ 系统日志：记录用户操作日志，查看系统执行日志记录
- ✔ 数据监控：定制Druid信息，提供简洁有效的SQL监控
- ✔ 聚合文档：定制在线文档，提供简洁美观的API文档
- ✔ 备份还原：系统备份还原，一键恢复系统初始化数据
- ✔ 主题切换：支持主题切换，自定主题颜色，一键换肤
- ✔ 服务监控：集成Spring Boot Admin，实现服务监控
- ✔ 代码生成：提供代码生成器，最大化的降低代码开发量
- ✘ 单点登录：利用 OAuth2, 提供统一的单点登录功能
- ✘ 系统登录：集成第三方登录功能（QQ、微信、微博）
- ...

### 软件架构

##### 开发环境

- IDE : Webstorm
- NODE: Node 8.9.x
- NPM : NPM 6.4.x

##### 技术选型

- 前端框架：Vue 2.x
- 页面组件：Element 2.x
- 状态管理：Vuex 2.x
- 后台交互：axios 0.18.x
- 图标使用：Font Awesome 4.x

##### 项目结构

zfw-ui
- assets： 图标、字体、国际化信息等静态信息
- components： 组件库，对常用组件进行封装
- http： 后台交互模块，统一后台接口请求API
- i18n： 国际化模块，使用Vue i18n进行国际化
- mock： Mock模块，模拟接口调用并返回定制数据
- permission： 权限控制模块，处理权限认证逻辑
- router： 路由管理模块，负责页面各种路由配置
- store： 状态管理模块，提供组件间状态共享
- utils： 工具模块，提供一些通用的工具方法
- views： 页面模块，主要放置各种页面视图组件

### 安装教程

1. 下载源码

    git clone https://gitee.com/w12w.com/zfw-ui

2. 编译代码

    进入项目根目录，执行 npm install, 下载和安装项目相关依赖包。

3. 启动系统

    执行 npm run dev 命令，启动项目，通过 http://localhost:8090 访问。

4. 项目打包

    执行 npm run build 命令，启动打包，完成之后会生成 dist 目录。

5. Mock 开关

    通过修改src/mock/index.js中的openMock变量，可以一键开启或关闭Mock功能。

6. 修改配置

    如果想自定义端口（默认是8090），可以修改 config/index.js 下的 port 属性。

    后台接口和备份服务器地址配置在 src/utils/global.js，如有修改请做相应变更。


    
### 系统展示

#### 登录界面

![输入图片说明](https://images.gitee.com/uploads/images/2018/1227/155412_c2f72f83_645970.png "屏幕截图.png")

#### 用户管理

![输入图片说明](https://images.gitee.com/uploads/images/2018/1227/154342_b8a9eaeb_645970.png "屏幕截图.png")

#### 机构管理

![输入图片说明](https://images.gitee.com/uploads/images/2018/1227/154417_7aefe6c2_645970.png "屏幕截图.png")

#### 角色管理

![输入图片说明](https://images.gitee.com/uploads/images/2018/1227/154431_6d8efe43_645970.png "屏幕截图.png")

#### 菜单管理

![输入图片说明](https://images.gitee.com/uploads/images/2018/1227/154446_61cd21a5_645970.png "屏幕截图.png")

#### 字典管理

![输入图片说明](https://images.gitee.com/uploads/images/2018/1227/154459_22ae6f2d_645970.png "屏幕截图.png")

#### 系统日志

![输入图片说明](https://images.gitee.com/uploads/images/2018/1227/154510_3c1bfd3f_645970.png "屏幕截图.png")

#### 数据监控

用户名：admin, 密码:admin，即服务端配置的密码

![输入图片说明](https://images.gitee.com/uploads/images/2018/1227/154556_8de439d5_645970.png "屏幕截图.png")

#### 服务监控

![输入图片说明](https://images.gitee.com/uploads/images/2018/1227/154701_6bcd25f2_645970.png "屏幕截图.png")

#### 注册中心

![输入图片说明](https://images.gitee.com/uploads/images/2018/1227/154734_877e893e_645970.png "屏幕截图.png")

#### 接口文档

![输入图片说明](https://images.gitee.com/uploads/images/2018/1227/154803_6a612548_645970.png "屏幕截图.png")

#### 代码生成

![输入图片说明](https://images.gitee.com/uploads/images/2018/1227/154840_18fc781c_645970.png "屏幕截图.png")

#### 主题切换

![输入图片说明](https://images.gitee.com/uploads/images/2018/1227/154943_1752c336_645970.png "屏幕截图.png")

### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)