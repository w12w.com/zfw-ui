import axios from '../axios'

/* 
 * 用户管理模块
 */

// 保存
export const save = (data) => {
    return axios({
        url: '/user/save',
        method: 'post',
        data
    })
};
// 删除
export const batchDelete = (data) => {
    return axios({
        url: '/user/delete',
        method: 'post',
        data
    })
};
// 分页查询
export const findPage = (data) => {
    return axios({
        url: '/user/findPage',
        method: 'post',
        data
    })
};
// 获取用户的菜单权限标识集合
export const findPermissions = (params) => {
    return axios({
        url: '/user/findPermissions',
        method: 'get',
        params
    })
};

// 获取当前登录用户的信息
export const findLoginUserMsg = () => {
  return axios({
    url: '/user/loginUser',
    method: 'get',
  })
};

// 获取当前登录用户的头像
export const findAvatar = () => {
  return axios({
    url: '/user/Avatar.png',
    method: 'get',
  })
};

// 更改当前登录用户的密码
export const updatePassword = (params) => {
  return axios({
    url: '/user/updatePassword',
    method: 'get',
    params
  });
};
// 更新当前登录用户的信息
export const updateLoginUser = (data) => {
  return axios({
    url: '/user/loginUser',
    method: 'put',
    data
  })
};

